USE TaskDev;

INSERT INTO taskGroup (taskGroupName, archived)
VALUES
    ('Bedroom', false),
    ('Study', true),
    ('Bathroom', false),
    ('Stairs/Hall', false),
    ('Kitchen', false),
    ('Dining Room', false),
    ('Living Room', false),
    ('Porch', false),
    ('Garden - Front', false),
    ('Garden - Back', false),
    ('Garden - Side', false),
    ('Misc', false);

INSERT INTO task(taskName, taskGroupId, idealFrequencyInDays, backupFrequencyInDays, archived)
    (SELECT 'Bed1',  g.id , 5, 10, false FROM taskGroup g WHERE g.taskGroupName = 'Bedroom');
INSERT INTO task(taskName, taskGroupId, idealFrequencyInDays, backupFrequencyInDays, archived)
    (SELECT 'Bed2',  g.id , 5, 10, false FROM taskGroup g WHERE g.taskGroupName = 'Bedroom');
INSERT INTO task(taskName, taskGroupId, idealFrequencyInDays, backupFrequencyInDays, archived)
    (SELECT 'Bed3',  g.id , 5, 10, false FROM taskGroup g WHERE g.taskGroupName = 'Bedroom');
INSERT INTO task(taskName, taskGroupId, idealFrequencyInDays, backupFrequencyInDays, archived)
    (SELECT 'Stu1',  g.id , 5, 10, false FROM taskGroup g WHERE g.taskGroupName = 'Study');
INSERT INTO task(taskName, taskGroupId, idealFrequencyInDays, backupFrequencyInDays, archived)
    (SELECT 'Stu2',  g.id , 5, 10, false FROM taskGroup g WHERE g.taskGroupName = 'Study');
INSERT INTO task(taskName, taskGroupId, idealFrequencyInDays, backupFrequencyInDays, archived)
    (SELECT 'Stu3',  g.id , 5, 10, false FROM taskGroup g WHERE g.taskGroupName = 'Study');
INSERT INTO task(taskName, taskGroupId, idealFrequencyInDays, backupFrequencyInDays, archived)
    (SELECT 'Bat1',  g.id , 5, 10, true FROM taskGroup g WHERE g.taskGroupName = 'Bathroom');
INSERT INTO task(taskName, taskGroupId, idealFrequencyInDays, backupFrequencyInDays, archived)
    (SELECT 'Bat2',  g.id , 5, 10, true FROM taskGroup g WHERE g.taskGroupName = 'Bathroom');
INSERT INTO task(taskName, taskGroupId, idealFrequencyInDays, backupFrequencyInDays, archived)
    (SELECT 'Bat3',  g.id , 5, 15, false FROM taskGroup g WHERE g.taskGroupName = 'Bathroom');

INSERT INTO colour (status, colour)
VALUES
    ('done', '187f18'),
    ('warning', 'ffc000'),
    ('danger', 'e71212'),
    ('blank', '00b6ff');


INSERT INTO taskDone (taskId, doneDate, fullyDone)
VALUES
    (1,'2002-07-21', TRUE),
    (1,'2002-06-21', TRUE),
    (1,'2002-08-21', TRUE),
    (1,'2002-08-22', FALSE),
    (2,'2002-08-21', FALSE),
    (2,'2002-08-22', TRUE);

