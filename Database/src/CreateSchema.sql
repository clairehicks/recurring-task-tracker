USE TaskDev;

CREATE TABLE IF NOT EXISTS taskGroup (
    id MEDIUMINT PRIMARY KEY AUTO_INCREMENT,
    taskGroupName TEXT UNIQUE,
    archived BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS task (
    id MEDIUMINT PRIMARY KEY AUTO_INCREMENT,
    taskName TEXT NOT NULL,
    taskGroupId MEDIUMINT NOT NULL,
    idealFrequencyInDays INT NOT NULL,
    backupFrequencyInDays INT NOT NULL,
    archived BOOLEAN NOT NULL,
    CONSTRAINT nameAndTaskGroupIdAreUnique UNIQUE (taskName, taskGroupId),
    CONSTRAINT taskGroupId_taskGroup_id FOREIGN KEY(taskGroupId) REFERENCES taskGroup(id),
    CONSTRAINT idealFrequencyInDaysPositive CHECK(idealFrequencyInDays>0),
    CONSTRAINT backupFrequencyInDaysGreaterThanIdealFrequencyInDays CHECK (backupFrequencyInDays>idealFrequencyInDays)
);

CREATE TABLE IF NOT EXISTS taskDone (
    id MEDIUMINT PRIMARY KEY AUTO_INCREMENT,
    taskId MEDIUMINT NOT NULL,
    doneDate DATE NOT NULL,
    fullyDone BOOLEAN NOT NULL,
    CONSTRAINT taskId_task_id FOREIGN KEY(taskId) REFERENCES task(id)
);

CREATE TABLE IF NOT EXISTS colour (
    status ENUM ('done', 'warning', 'danger', 'blank') NOT NULL UNIQUE,
    colour CHAR(6) -- eg store #123456 stored as 123456
);

CREATE VIEW taskLastDone AS (
    SELECT taskGroup.id AS taskGroupId, taskGroupName, task.id AS taskId, taskName, taskFullyDone,
	 	( CASE WHEN taskPartiallyDone > taskFullyDone THEN taskPartiallyDone ELSE null END) AS taskPartiallyDone
    FROM taskGroup RIGHT JOIN task ON taskGroup.id = task.taskGroupId
    LEFT JOIN (
	 	SELECT taskDone.taskId, MAX(taskDone.doneDate) AS taskFullyDone FROM taskDone WHERE taskDone.fullyDone = TRUE GROUP BY taskDone.taskId) AS taskFullyDone
    	ON taskFullyDone.taskId = task.id
     LEFT JOIN (
	 	SELECT taskDone.taskId, MAX(taskDone.doneDate) AS taskPartiallyDone FROM taskDone WHERE taskDone.fullyDone = FALSE GROUP BY taskDone.taskId) AS taskPartiallyDone
    	ON taskPartiallyDone.taskId = task.id
)